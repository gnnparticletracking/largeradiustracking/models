import sys, os
import copy
import logging

from pytorch_lightning.callbacks import Callback
import torch.nn.functional as F
import sklearn.metrics
import matplotlib.pyplot as plt
import torch
import numpy as np

"""
Class-based Callback inference for integration with Lightning
"""


class GNNInferenceCallback(Callback):
    def __init__(self):
        self.output_dir = None
        self.overwrite = False

    def on_test_start(self, trainer, pl_module):
        # Prep the directory to produce inference data to
        self.output_dir = pl_module.hparams.output_dir
        self.datatypes = ["train", "val", "test"]
        os.makedirs(self.output_dir, exist_ok=True)
        [
            os.makedirs(os.path.join(self.output_dir, datatype), exist_ok=True)
            for datatype in self.datatypes
        ]

    def on_test_end(self, trainer, pl_module):
        print("Training finished, running inference to filter graphs...")

        # By default, the set of examples propagated through the pipeline will be train+val+test set
        datasets = {
            "train": pl_module.trainset,
            "val": pl_module.valset,
            "test": pl_module.testset,
        }
        total_length = sum([len(dataset) for dataset in datasets.values()])
        batch_incr = 0

        pl_module.eval()
        with torch.no_grad():
            for set_idx, (datatype, dataset) in enumerate(datasets.items()):
                for batch_idx, batch in enumerate(dataset):
                    percent = (batch_incr / total_length) * 100
                    sys.stdout.flush()
                    sys.stdout.write(f"{percent:.01f}% inference complete \r")
                    if (
                        not os.path.exists(
                            os.path.join(
                                self.output_dir, datatype, batch.event_file[-4:]
                            )
                        )
                    ) or self.overwrite:
                        batch = batch.to(pl_module.device)  # Is this step necessary??
                        batch = self.construct_downstream(batch, pl_module)
                        self.save_downstream(batch, pl_module, datatype)

                    batch_incr += 1

    def construct_downstream(self, batch, pl_module):

        emb = (
            None if (pl_module.hparams["emb_channels"] == 0) else batch.embedding
        )  # Does this work??

        output = (
            pl_module(
                torch.cat([batch.cell_data, batch.x], axis=-1), batch.e_radius, emb
            ).squeeze()
            if ("ci" in pl_module.hparams["regime"])
            else pl_module(batch.x, batch.e_radius, emb).squeeze()
        )
        y_pid = batch.pid[batch.e_radius[0]] == batch.pid[batch.e_radius[1]]

        cut_indices = F.sigmoid(output) > pl_module.hparams["filter_cut"]
        batch.e_radius = batch.e_radius[:, cut_indices]
        batch.y_pid = y_pid[cut_indices]
        batch.y = batch.y[cut_indices]

        return batch

    def save_downstream(self, batch, pl_module, datatype):

        with open(
            os.path.join(self.output_dir, datatype, batch.event_file[-4:]), "wb"
        ) as pickle_file:
            torch.save(batch, pickle_file)


class GNNTelemetry(Callback):

    """
    This callback contains standardised tests of the performance of a GNN
    """

    def __init__(self):
        super().__init__()
        logging.info("CONSTRUCTING CALLBACK!")

    def on_test_start(self, trainer, pl_module):

        """
        This hook is automatically called when the model is tested after training. The best checkpoint is automatically loaded
        """
        self.preds = []
        self.truth = []
        self.score = []

    def on_test_batch_end(
        self, trainer, pl_module, outputs, batch, batch_idx, dataloader_idx
    ):

        """
        Get the relevant outputs from each batch
        """

        self.preds.append(outputs["preds"].detach().cpu())
        self.truth.append(outputs["truth"].detach().cpu())
        self.score.append(outputs["score"].detach().cpu())

    def on_test_end(self, trainer, pl_module):

        """
        1. Aggregate all outputs,
        2. Calculate the ROC curve,
        3. Plot ROC curve,
        4. Save plots to PDF 'metrics.pdf'
        """

        # REFACTOR THIS INTO CALCULATE METRICS, PLOT METRICS, SAVE METRICS
        preds = np.concatenate(self.preds)
        truth = np.concatenate(self.truth)
        score = np.concatenate(self.score)
        print(preds.shape, truth.shape)

        roc_fpr, roc_tpr, roc_thresholds = sklearn.metrics.roc_curve(truth, score)
        roc_auc = sklearn.metrics.auc(roc_fpr, roc_tpr)
        logging.info("ROC AUC: %s", roc_auc)

        true = truth.sum()
        true_positive = (truth.astype(bool) & preds).sum()
        positive = preds.sum()

        print('eff:', true_positive/true)
        print('pur:', true_positive/positive)

        # Update this to dynamically adapt to number of metrics
        fig, axs = plt.subplots(nrows=1, ncols=1, figsize=(8, 8))
        axs = axs.flatten() if type(axs) is list else [axs]

        axs[0].plot(roc_fpr, roc_tpr)
        axs[0].plot([0, 1], [0, 1], "--")
        axs[0].set_xlabel("False positive rate")
        axs[0].set_ylabel("True positive rate")
        axs[0].set_title("ROC curve, AUC = %.3f" % roc_auc)
        plt.tight_layout()

        fig.savefig("metrics.pdf", format="pdf")

        fig, ax = plt.subplots(nrows=1, ncols=1, figsize=(8, 8))
        ax.hist(
            score[truth > 0.5],
            bins=50,
            log=True,
            range=(0, 1),
            histtype='step',
            label='True Edges'
        )
        ax.hist(
            score[truth <= 0.5],
            bins=50,
            log=True,
            range=(0, 1),
            histtype='step',
            label='False Edges'
        )
        ax.legend()

        ax.set_xlabel("Arbitrary Scale")
        ax.set_ylabel("Model Output")
        ax.set_title("Score Distribution")
        plt.tight_layout()

        fig.savefig("score_dist.pdf", format="pdf")


class GNNBuilder(Callback):
    """Callback handling filter inference for later stages.

    This callback is used to apply a trained filter model to the dataset of a LightningModule.
    The data structure is preloaded in the model, as training, validation and testing sets.
    Intended usage: run training and examine the telemetry to decide on the hyperparameters (e.g. r_test) that
    lead to desired efficiency-purity tradeoff. Then set these hyperparameters in the pipeline configuration and run
    with the --inference flag. Otherwise, to just run straight through automatically, train with this callback included.

    """

    def __init__(self):
        self.output_dir = None
        self.overwrite = False

    def on_test_end(self, trainer, pl_module):

        print("Testing finished, running inference to build graphs...")

        datasets = self.prepare_datastructure(pl_module)

        total_length = sum([len(dataset) for dataset in datasets.values()])

        pl_module.eval()
        with torch.no_grad():
            batch_incr = 0
            for set_idx, (datatype, dataset) in enumerate(datasets.items()):
                for batch_idx, batch in enumerate(dataset):
                    percent = (batch_incr / total_length) * 100
                    sys.stdout.flush()
                    sys.stdout.write(f"{percent:.01f}% inference complete \r")
                    if (
                        not os.path.exists(
                            os.path.join(
                                self.output_dir, datatype, batch.event_file[-4:]
                            )
                        )
                    ) or self.overwrite:
                        batch_to_save = copy.deepcopy(batch)
                        batch_to_save = batch_to_save.to(
                            pl_module.device
                        )  # Is this step necessary??
                        self.construct_downstream(batch_to_save, pl_module, datatype)

                    batch_incr += 1

    def prepare_datastructure(self, pl_module):
        # Prep the directory to produce inference data to
        self.output_dir = pl_module.hparams.output_dir
        self.datatypes = ["train", "val", "test"]

        os.makedirs(self.output_dir, exist_ok=True)
        [
            os.makedirs(os.path.join(self.output_dir, datatype), exist_ok=True)
            for datatype in self.datatypes
        ]

        # Set overwrite setting if it is in config
        self.overwrite = (
            pl_module.hparams.overwrite if "overwrite" in pl_module.hparams else False
        )

        # By default, the set of examples propagated through the pipeline will be train+val+test set
        datasets = {
            "train": pl_module.trainset,
            "val": pl_module.valset,
            "test": pl_module.testset,
        }

        return datasets

    def construct_downstream(self, batch, pl_module, datatype):

        outputs = pl_module.shared_evaluation(batch, 0, log=False)

        # output = pl_module(
        #     pl_module.get_input_data(batch),
        #     torch.cat([batch.edge_index, batch.edge_index.flip(0)], dim=-1),
        # ).squeeze()
        # batch.scores = torch.sigmoid(output)

        batch.score = outputs["score"].detach().cpu()
        batch.truth = outputs["truth"].detach().cpu()
        batch.edge_index = outputs["edges"].detach().cpu()
        
        self.save_downstream(batch, pl_module, datatype)

    def save_downstream(self, batch, pl_module, datatype):

        with open(
            os.path.join(self.output_dir, datatype, batch.event_file[-4:]), "wb"
        ) as pickle_file:
            torch.save(batch, pickle_file)

        logging.info("Saved event {}".format(batch.event_file[-4:]))

