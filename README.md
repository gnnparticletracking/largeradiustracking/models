# Models

This repo contains train configuration and code for models.
It should be used with [Exa.TrkX pipeline](https://github.com/HSF-reco-and-software-triggers/Tracking-ML-Exa.TrkX).
See [Documentation](https://gnnparticletracking.gitlab.io/largeradiustracking/docs/subprojects/models/) for more information.


## License

Model source code are belong to Exa.TrkX group.
It is contained in this repo for archiving and future reference.

